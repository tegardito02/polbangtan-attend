// start: Sidebar
const sidebarToggle = document.querySelector('.sidebar-toggle')
const sidebarOverlay = document.querySelector('.sidebar-overlay')
const sidebarMenu = document.querySelector('.sidebar-menu')
const main = document.querySelector('.main')
sidebarToggle.addEventListener('click', function (e) {
    e.preventDefault()
    sidebarMenu.classList.toggle('-translate-x-full')
    sidebarOverlay.classList.toggle('hidden');
    main.classList.toggle('active');
})
sidebarOverlay.addEventListener('click', function (e) {
    e.preventDefault()
    main.classList.add('active')
    sidebarOverlay.classList.add('hidden')
    sidebarMenu.classList.add('-translate-x-full')
})
document.querySelectorAll('.sidebar-dropdown-toggle').forEach(function (item) {
    item.addEventListener('click', function (e) {
        e.preventDefault()
        const parent = item.closest('.group')
        if (parent.classList.contains('selected')) {
            parent.classList.remove('selected')
        } else {
            document.querySelectorAll('.sidebar-dropdown-toggle').forEach(function (i) {
                i.closest('.group').classList.remove('selected')
            })
            parent.classList.add('selected')
        }
    })
})
// end: Sidebar



// start: Popper
const popperInstance = {}
document.querySelectorAll('.dropdown').forEach(function (item, index) {
    const popperId = 'popper-' + index
    const toggle = item.querySelector('.dropdown-toggle')
    const menu = item.querySelector('.dropdown-menu')
    menu.dataset.popperId = popperId
    popperInstance[popperId] = Popper.createPopper(toggle, menu, {
        modifiers: [
            {
                name: 'offset',
                options: {
                    offset: [0, 8],
                },
            },
            {
                name: 'preventOverflow',
                options: {
                    padding: 24,
                },
            },
        ],
        placement: 'bottom-end'
    });
})
document.addEventListener('click', function (e) {
    const toggle = e.target.closest('.dropdown-toggle')
    const menu = e.target.closest('.dropdown-menu')
    if (toggle) {
        const menuEl = toggle.closest('.dropdown').querySelector('.dropdown-menu')
        const popperId = menuEl.dataset.popperId
        if (menuEl.classList.contains('hidden')) {
            hideDropdown()
            menuEl.classList.remove('hidden')
            showPopper(popperId)
        } else {
            menuEl.classList.add('hidden')
            hidePopper(popperId)
        }
    } else if (!menu) {
        hideDropdown()
    }
})

function hideDropdown() {
    document.querySelectorAll('.dropdown-menu').forEach(function (item) {
        item.classList.add('hidden')
    })
}
function showPopper(popperId) {
    popperInstance[popperId].setOptions(function (options) {
        return {
            ...options,
            modifiers: [
                ...options.modifiers,
                { name: 'eventListeners', enabled: true },
            ],
        }
    });
    popperInstance[popperId].update();
}
function hidePopper(popperId) {
    popperInstance[popperId].setOptions(function (options) {
        return {
            ...options,
            modifiers: [
                ...options.modifiers,
                { name: 'eventListeners', enabled: false },
            ],
        }
    });
}
// end: Popper

// start: remove active main
function handleMediaChange(mediaQuery) {
    const mainElement = document.querySelector('.main');
    
    if (mediaQuery.matches) {
      mainElement.classList.remove('active');
    } else {
      mainElement.classList.add('active');
    }
  }
  
  const mediaQuery = window.matchMedia('(min-width: 768px)');
  handleMediaChange(mediaQuery);
  mediaQuery.addListener(handleMediaChange);
  // end: remove active main



// start: Tab
document.querySelectorAll('[data-tab]').forEach(function (item) {
    item.addEventListener('click', function (e) {
        e.preventDefault()
        const tab = item.dataset.tab
        const page = item.dataset.tabPage
        const target = document.querySelector('[data-tab-for="' + tab + '"][data-page="' + page + '"]')
        document.querySelectorAll('[data-tab="' + tab + '"]').forEach(function (i) {
            i.classList.remove('active')
        })
        document.querySelectorAll('[data-tab-for="' + tab + '"]').forEach(function (i) {
            i.classList.add('hidden')
        })
        item.classList.add('active')
        target.classList.remove('hidden')
    })
})
// end: Tab



// start: kalender
var keluar = [
    {
      title: 'Keluar',
      start: '2023-09-01', // Tanggal masuk presensi
    },
    // Tambahkan acara lainnya di sini
  ];

  document.addEventListener('DOMContentLoaded', function() {
    var calendarEl = document.getElementById('calendar');
    var calendar = new FullCalendar.Calendar(calendarEl, {
        height: 450,
        initialView: 'dayGridMonth',
        headerToolbar: {
            left: 'prev,next', // Hanya menyertakan tombol prev dan next
            center: 'title',
            right: ''
        },
        events: keluar, // Menggabungkan array masuk dan keluar
        eventContent: function(arg) {
            var eventEl = document.createElement('div');
            eventEl.innerHTML = arg.event.title;

            // Menggunakan kelas-kelas Tailwind untuk mengatur tampilan
            eventEl.className = ' px-1 text-white text-center text-sm';

            // Atur warna latar belakang dan teks berdasarkan judul acara
            eventEl.classList.add('bg-red-500')

            return { domNodes: [eventEl] };
        }
    });
    calendar.render();
});

// end: kalender


// // start: Chart
// new Chart(document.getElementById('order-chart'), {
//     type: 'line',
//     data: {
//         labels: generateNDays(7),
//         datasets: [
//             {
//                 label: 'Active',
//                 data: generateRandomData(7),
//                 borderWidth: 1,
//                 fill: true,
//                 pointBackgroundColor: 'rgb(59, 130, 246)',
//                 borderColor: 'rgb(59, 130, 246)',
//                 backgroundColor: 'rgb(59 130 246 / .05)',
//                 tension: .2
//             },
//             {
//                 label: 'Completed',
//                 data: generateRandomData(7),
//                 borderWidth: 1,
//                 fill: true,
//                 pointBackgroundColor: 'rgb(16, 185, 129)',
//                 borderColor: 'rgb(16, 185, 129)',
//                 backgroundColor: 'rgb(16 185 129 / .05)',
//                 tension: .2
//             },
//             {
//                 label: 'Canceled',
//                 data: generateRandomData(7),
//                 borderWidth: 1,
//                 fill: true,
//                 pointBackgroundColor: 'rgb(244, 63, 94)',
//                 borderColor: 'rgb(244, 63, 94)',
//                 backgroundColor: 'rgb(244 63 94 / .05)',
//                 tension: .2
//             },
//         ]
//     },
//     options: {
//         scales: {
//             y: {
//                 beginAtZero: true
//             }
//         }
//     }
// });

// function generateNDays(n) {
//     const data = []
//     for(let i=0; i<n; i++) {
//         const date = new Date()
//         date.setDate(date.getDate()-i)
//         data.push(date.toLocaleString('en-US', {
//             month: 'short',
//             day: 'numeric'
//         }))
//     }
//     return data
// }
// function generateRandomData(n) {
//     const data = []
//     for(let i=0; i<n; i++) {
//         data.push(Math.round(Math.random() * 10))
//     }
//     return data
// }
// // end: Chart
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>E-Absen Asrama Polbangtan-mlg</title>
    <link rel="icon" href="{{ asset('img/logo-polbangtan2.png') }}">
    <link rel="stylesheet" href="{{ mix('css/login.css') }}">
    @vite('resources/css/app.css')
    <script src="{{ mix('js/main.js') }}"></script>
    <script src="{{ asset('js/script.js') }}"></script>
</head>
<body>
    <div class=" bg-gray-200 px-6 h-screen">
        <div class="container ml-0 md:ml-5 flex justify-between">
            <!-- Logo dan Nama -->
            <div class="mt-4 flex items-center">
                <a href="" class="flex items-center">
                    <img src="{{ asset('img/logo-polbangtan2.png') }}" alt="Logo" class="h-10 w-10 mr-2">
                    <div class="text-gray-700 text-xl font-bold">E-Absen Asrama Polbangtan-mlg</div>
                </a>
            </div>
        </div>
        <div class="flex justify-center items-center mt-[10%]">
            <div class="p-6 max-w-sm w-full bg-white shadow-md rounded-md">
                <div class="flex justify-center items-center">
                    <img src="{{ asset('img/logo-polbangtan2.png') }}" alt="Logo" class="h-10 w-10 mr-2">
                    <span class="text-gray-700 text-center font-semibold text-2xl">E-Absen Dashboard</span>
                </div>
        
                <form class="mt-4" action="" method="GET">
                    <label class="block">
                        <span class="text-gray-700 text-sm">Email</span>
                        <input type="email" class="form-input mt-1 block w-full p-1 rounded-md border-2 shadow border-utama">
                    </label>
        
                    <label class="block mt-3">
                        <span class="text-gray-700 text-sm">Password</span>
                        <input type="password" class="form-input mt-1 block w-full p-1 rounded-md border-2 shadow border-utama">
                    </label>
        
                    <div class="flex justify-between items-center mt-4">
                        <div>
                            <label class="inline-flex items-center">
                                <input type="checkbox" class="form-checkbox text-utama">
                                <span class="mx-2 text-gray-600 text-sm">Remember me</span>
                            </label>
                        </div>
                        
                        <div>
                            <a class="block text-sm fontme text-utama hover:underline" href="#">Lupa password?</a>
                        </div>
                    </div>
        
                    <div class="mt-6">
                        <button class="py-2 px-4 text-center bg-utama rounded-md w-full text-white text-sm hover:bg-teal-800 transition-all">
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
        
    </div>
</body>

</html>
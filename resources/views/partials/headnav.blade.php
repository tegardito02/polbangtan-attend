{{-- Start: HeadNav --}}
<div class="py-2 px-6 bg-white flex items-center shadow-md shadow-black/5 sticky top-0 left-0 z-30">
    <button type="button" class="text-lg text-gray-600 md:hidden sidebar-toggle">
        <i class="ri-menu-line"></i>
    </button>
    <ul class="flex items-center text-sm ml-4 md:ml-0">
        <li class="mr-2">
            <a href="#" class="text-gray-400 hover:text-gray-600 font-medium">Dashboard</a>
        </li>
        <li class="text-gray-600 mr-2 font-medium">/</li>
        <li class="text-gray-600 mr-2 font-medium">Home {{-- nanti dibagian ini diberikan varibel sesuai halamannya --}}</li>
    </ul>
    <ul class="ml-auto flex items-center">
        <li class="dropdown">
            <button type="button" class="dropdown-toggle text-gray-400 w-8 h-8 rounded flex items-center justify-center hover:bg-gray-50 hover:text-gray-600">
                <i class="ri-notification-3-line"></i>
            </button>
            <div class="dropdown-menu shadow-md shadow-black/5 z-30 hidden max-w-xs w-full bg-white rounded-md border border-gray-100">
                <div class="flex items-center px-4 pt-4 border-b border-b-gray-100 notification-tab">
                    <div class="text-utama font-medium text-[13px] hover:text-teal-600 border-b-2 border-b-transparent mr-4 pb-1 active">Notifications</div>
                </div>
                <div class="my-2">
                    <ul class="max-h-64 overflow-y-auto">
                        <li>
                            <a href="#" class="py-2 px-4 flex items-center hover:bg-gray-50 group">
                                <div class="ml-2">
                                    <div class="text-[13px] text-gray-600 font-medium truncate group-hover:text-utama">Informasi</div>
                                    <div class="text-[11px] text-gray-400">Dari Naufal</div>
                                </div>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </li>
        <li class="dropdown ml-3">
            <button type="button" class="dropdown-toggle flex items-center">
                <img src="https://placehold.co/32x32" alt="" class="w-8 h-8 rounded block object-cover align-middle">
            </button>
            <ul class="dropdown-menu shadow-md shadow-black/5 z-30 hidden py-1.5 rounded-md bg-white border border-gray-100 w-full max-w-[140px]">
                <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-utama hover:bg-gray-50">Profil</a>
                </li>
                <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-utama hover:bg-gray-50">Settings</a>
                </li>
                <li>
                    <a href="#" class="flex items-center text-[13px] py-1.5 px-4 text-gray-600 hover:text-utama hover:bg-gray-50">Logout</a>
                </li>
            </ul>
        </li>
    </ul>
</div>
{{-- end: HeadNav --}}
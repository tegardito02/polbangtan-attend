<div class="fixed left-0 top-0 w-64 h-full bg-teal-800 p-4 z-50 sidebar-menu transition-transform -translate-x-full md:-translate-x-0">
    <a href="#" class="flex items-center pb-4 border-b border-b-teal-900">
        <img src="{{ asset('img/kotak-polbangtan.png') }}" alt="" class="w-8 h-8 rounded object-cover">
        <span class="text-lg font-bold text-white ml-3">E-Absen Asrama</span>
    </a>
    <ul class="mt-4">
        <li class="mb-1 group active">
            <a href="/dashboard" class="{{ Request::is('dashboard') ? 'text-white bg-utama' : 'text-gray-300 hover:bg-teal-950 hover:text-gray-100' }} flex items-center py-2 px-4 rounded-md">
                <i class="ri-home-2-line mr-3 text-lg"></i>
                <span class="text-sm">Dashboard</span>
            </a>
        </li>
        <li class="mb-1 group">
            <a href="#" class="{{ Request::is('dashboard/kode-qr') ? 'text-white bg-utama' : 'text-gray-300 hover:bg-teal-950 hover:text-gray-100' }} flex items-center py-2 px-4 rounded-md">
                <i class="ri-qr-code-line mr-3 text-lg"></i>
                <span class="text-sm">Kode QR</span>
            </a>
        </li>
        <li class="mb-1 group">
            <a href="/dashboard/profil" class="{{ Request::is('dashboard/profil') ? 'text-white bg-utama' : 'text-gray-300 hover:bg-teal-950 hover:text-gray-100' }} flex items-center py-2 px-4 rounded-md">
                <i class="ri-user-line mr-3 text-lg"></i>
                <span class="text-sm">Profil</span>
            </a>
        </li>
        
        <li class="mb-1 group">
            <a href="#" class="{{ Request::is('dashboard/riwayat') ? 'text-white bg-utama' : 'text-gray-300 hover:bg-teal-950 hover:text-gray-100' }} flex items-center py-2 px-4 rounded-md">
                <i class="ri-history-line mr-3 text-lg"></i>
                <span class="text-sm">Riwayat Absensi</span>
            </a>
        </li>
        
        {{-- <li class="mb-1 group">
            <a href="#" class="flex items-center py-2 px-4 text-gray-300 hover:bg-teal-950 hover:text-gray-100 rounded-md group-[.active]:bg-utama group-[.active]:text-white group-[.selected]:bg-teal-950 group-[.selected]:text-gray-100">
                <i class="ri-settings-2-line mr-3 text-lg"></i>
                <span class="text-sm">Pengaturan</span>
            </a>
        </li> --}}
    </ul>
</div>
<div class="fixed top-0 left-0 w-full h-full bg-black/50 z-40 hidden md:hidden sidebar-overlay"></div>
@extends('layouts.main')
@section('container')
<!-- start: Main -->
<div class="p-6">
    <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3 gap-6 mb-6">
        <div class="bg-white rounded-md border border-gray-100 p-6 shadow-md shadow-black/5">
            <div class="flex justify-between">
                <div>
                    <div class="text-xl font-semibold mb-12">Pengguna Aktif</div>
                    <div class="text-sm font-medium text-green-700 flex items-center">
                        <span class="relative flex h-3 w-3 mr-2">
                            <span class="animate-ping absolute inline-flex h-full w-full rounded-full bg-green-500 opacity-75"></span>
                            <span class="relative inline-flex rounded-full h-3 w-3 bg-green-500"></span>
                        </span>
                        10 Pengguna
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-white rounded-md border border-gray-100 p-6 shadow-md shadow-black/5">
            <div class="flex justify-between">
                <div>
                    <div class="text-xl font-semibold mb-12">Total Pengguna</div>
                    <div class="text-sm font-medium text-amber-700 flex items-center">
                        <div class="w-3 h-3 rounded-full bg-amber-500 animate-pulse mr-2"></div>
                        343 Pengguna
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-white rounded-md border border-gray-100 p-6 shadow-md shadow-black/5">
            <div class="flex justify-between">
                <div>
                    <div class="text-xl font-semibold mb-12">Piket pada hari ini</div>
                    <div class="text-sm font-medium text-utama flex items-center">
                        Naufal S.Pd.
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="flex flex-col justify-between gap-5">
        <div class="bg-white border border-gray-100 shadow-md shadow-black/5 p-6 rounded-md">
            <div class="flex justify-between mb-4 items-start">
                <div class="font-medium">Status Presensi</div>
            </div>
            <div class="overflow-x-auto">
                <!-- Alasan Presensi -->
                <div class="flex flex-col md:flex-row justify-normal md:justify-between">
                    <!-- Icon -->
                    <div class="flex items-center">
                        <div class="bg-red-500 p-2 rounded-full mr-3">
                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" class="h-6 w-6 text-white">
                                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12"></path>
                            </svg>
                        </div>
                        <!-- Keterangan Alasan -->
                        <div>
                            <h3 class="text-lg font-semibold">Diluar Asrama</h3>
                            <p class="text-gray-600 text-sm">Alasan: Lagi keluar bersama teman - teman untuk menjalan tugas matkul blablabla</p>
                        </div>
                    </div>
                    <div class="text-sm mt-5 md:mt-0 text-gray-500">
                        Batas Waktu Keluar:<br> 
                        Jam 06.00 WIB - Jam 20.00 WIB
                    </div> 
                </div>
            </div>
        </div>
    </div>
    <div class="grid grid-cols-1 lg:grid-cols-3 gap-6 mb-6 mt-6">
        <div class="bg-white border border-gray-100 shadow-md shadow-black/5 p-6 rounded-md lg:col-span-2">
            <div class="flex justify-between mb-4 items-start">
                <div class="font-medium">Kalender Stastik</div>
            </div>
            <div class="overflow-x-auto">
                <div id="calendar" class="m-2 text-sm md:text-lg"></div> 
            </div>   
        </div>
        <div class="bg-white border border-gray-100 shadow-md shadow-black/5 p-6 rounded-md h-[150px]">
            <div class="flex justify-between mb-4 items-start">
                <div class="font-medium">Berikut detail rekapan Anda</div>
            </div>
            <div class="overflow-x-auto">
                <div class="w-full bg-utama rounded">
                    <ul class="list-group">
                        <!-- Data "Keluar" -->
                        <li class="list-group-item flex justify-between items-center p-3">
                            <span class="text-white text-sm">Keluar</span>
                            <span class="badge bg-red-500 text-sm text-white p-1 rounded">1 <small>Hari</small></span>
                        </li>
                        <!-- Tambahkan data lainnya di sini -->
                    </ul>
                </div> 
            </div>
        </div>
    </div>
</div>
<!-- end: Main -->
@endsection